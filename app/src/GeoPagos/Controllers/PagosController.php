<?php namespace GeoPagos\Controllers;
/**
 * @author Ricardo Bianchi <ericardo.bianchi@gmail.com>
 */

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use GeoPagos\Controllers\GeoPagosController;
use GeoPagos\Models\Services\PagoService;
use GeoPagos\Models\Entities\Pago;
use GeoPagos\Models\Exception\ServiceException;

class PagosController extends GeoPagosController {

	protected $pagoService = null;

	public function __construct()
	{
		parent::__construct();
		
		$this->pagoService = new PagoService();
	}

	public function listPagos()
	{
		try {
			$pagos = $this->pagoService->listPagos();
		} catch( ServiceException $e ) {
			return Response::json(array('status'=>'FAIL', 'message'=>$e->getMessage()));
		}
		return Response::json(array('status'=>'OK', 'pagos'=>$pagos));
	}

	public function add()
	{
		$data = Input::all();
		$pago = new Pago();
		$codigousuario = $data['codigousuario'];
		$pago->setImporte($data['importe']);
		$pago->setFecha($data['fecha']);

		try {
			$this->pagoService->add($pago, $codigousuario);
		} catch( ServiceException $e ) {
			return Response::json(array('status'=>'FAIL', 'message'=>$e->getMessage()));
		}

		return Response::json(array('status'=>'OK'));
	}

	public function remove()
	{
		$data = Input::all();
		$codigopago = $data['codigopago'];

		try {
			$this->pagoService->remove($codigopago);
		} catch( ServiceException $e ) {
			return Response::json(array('status'=>'FAIL', 'message'=>$e->getMessage()));
		}

		return Response::json(array('status'=>'OK'));
	}

}
