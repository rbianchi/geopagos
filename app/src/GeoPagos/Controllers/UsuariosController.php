<?php namespace GeoPagos\Controllers;
/**
 * @author Ricardo Bianchi <ericardo.bianchi@gmail.com>
 */

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use GeoPagos\Controllers\GeoPagosController;
use GeoPagos\Models\Services\UsuarioService;
use GeoPagos\Models\Entities\Usuario;
use GeoPagos\Models\Exception\ServiceException;

class UsuariosController extends GeoPagosController {

	protected $usuarioService = null;

	public function __construct()
	{
		parent::__construct();
		
		$this->usuarioService = new UsuarioService();
	}

	public function listUsers()
	{
		try {
			$users = $this->usuarioService->listUsers();
		} catch( ServiceException $e ) {
			return Response::json(array('status'=>'FAIL', 'message'=>$e->getMessage()));
		}
		return Response::json(array('status'=>'OK', 'users'=>$users));
	}

	public function add()
	{
		$data = Input::all();
		$usuario = new Usuario();
		$usuario->setUsuario($data['usuario']);
		$usuario->setClave($data['clave']);
		$usuario->setEdad($data['edad']);

		try {
			$this->usuarioService->add($usuario);
		} catch( ServiceException $e ) {
			return Response::json(array('status'=>'FAIL', 'message'=>$e->getMessage()));
		}

		return Response::json(array('status'=>'OK'));
	}

	public function remove()
	{
		$data = Input::all();
		$codigousuario = $data['codigousuario'];

		try {
			$this->usuarioService->remove($codigousuario);
		} catch( ServiceException $e ) {
			return Response::json(array('status'=>'FAIL', 'message'=>$e->getMessage()));
		}

		return Response::json(array('status'=>'OK'));
	}

	public function update()
	{
		$data = Input::all();
		$usuario = new Usuario();
		$usuario->setCodigoUsuario($data['codigousuario']);
		$usuario->setClave($data['clave']);
		$usuario->setEdad($data['edad']);

		try {
			$this->usuarioService->update($usuario);
		} catch( ServiceException $e ) {
			return Response::json(array('status'=>'FAIL', 'message'=>$e->getMessage()));
		}

		return Response::json(array('status'=>'OK'));
	}
		
}
