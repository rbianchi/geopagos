<?php namespace GeoPagos\Controllers;
/**
 * @author Ricardo Bianchi <ericardo.bianchi@gmail.com>
 */

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use GeoPagos\Controllers\GeoPagosController;
use GeoPagos\Models\Services\FavoritoService;
use GeoPagos\Models\Entities\Favorito;
use GeoPagos\Models\Exception\ServiceException;

class FavoritosController extends GeoPagosController {

	protected $favoritoService = null;

	public function __construct()
	{
		parent::__construct();
		
		$this->favoritoService = new FavoritoService();
	}

	public function listFavoritos()
	{
		try {
			$favoritos = $this->favoritoService->listFavoritos();
		} catch( ServiceException $e ) {
			return Response::json(array('status'=>'FAIL', 'message'=>$e->getMessage()));
		}
		return Response::json(array('status'=>'OK', 'favoritos'=>$favoritos));
	}

	public function add()
	{
		$data = Input::all();
		$favorito = new Favorito();
		$favorito->setCodigousuario($data['codigousuario']);
		$favorito->setCodigousuariofavorito($data['codigousuariofavorito']);

		try {
			$this->favoritoService->add($favorito);
		} catch( ServiceException $e ) {
			return Response::json(array('status'=>'FAIL', 'message'=>$e->getMessage()));
		}

		return Response::json(array('status'=>'OK'));
	}

	public function remove()
	{
		$data = Input::all();
		$favorito = new Favorito();
		$favorito->setCodigousuario($data['codigousuario']);
		$favorito->setCodigousuariofavorito($data['codigousuariofavorito']);

		try {
			$this->favoritoService->remove($favorito);
		} catch( ServiceException $e ) {
			return Response::json(array('status'=>'FAIL', 'message'=>$e->getMessage()));
		}

		return Response::json(array('status'=>'OK'));
	}

}
