<?php namespace GeoPagos\Controllers;
/**
 * @author Ricardo Bianchi <rbianchi@easytech.com.ar>
 */

use GeoPagos\Controllers\BaseController;
use Illuminate\Support\Facades\HTML;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Config;

class GeoPagosController extends BaseController {

    public function __construct()
    {
    }

    public static function style($url, $attributes = array())
    {
        return HTML::style($url, $attributes);
    }

    public static function script($url, $attributes = array())
    {
        return HTML::script($url, $attributes);
    }

	public function getEnvironment()
	{
		return app()->environment();
	}
}
