<?php namespace GeoPagos\Models\Entities;         

/**
 * @author Ricardo Bianchi <ericardo.bianchi@gmail.com>
 */

use Illuminate\Database\Eloquent\Model;
class UsuarioPago extends Model {

    protected $table = 'UsuariosPagos';

    public $codigopago;
    public $codigousuario;

    public function getCodigopago()
    {
    	return $this->codigopago;
    }

    public function getCodigousuario()
    {
    	return $this->codigousuario;
    }

    public function setCodigopago($codigopago)
    {
    	return $this->codigopago = $codigopago;
    }

    public function setCodigousuario($codigousuario)
    {
        return $this->codigousuario = $codigousuario;
    }

}
