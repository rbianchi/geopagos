<?php namespace GeoPagos\Models\Entities;         

/**
 * @author Ricardo Bianchi <ericardo.bianchi@gmail.com>
 */

use Illuminate\Database\Eloquent\Model;
class Favorito extends Model {

    protected $table = 'Favoritos';

    public $codigousuario;
    public $codigousuariofavorito;


    public function getCodigousuario()
    {
    	return $this->codigousuario;
    }

    public function setCodigousuario($codigousuario)
    {
    	return $this->codigousuario = $codigousuario;
    }

    public function getCodigousuariofavorito()
    {
        return $this->codigousuariofavorito;
    }

    public function setCodigousuariofavorito($codigousuariofavorito)
    {
        return $this->codigousuariofavorito = $codigousuariofavorito;
    }

}
