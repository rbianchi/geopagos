<?php namespace GeoPagos\Models\Entities;         

/**
 * @author Ricardo Bianchi <ericardo.bianchi@gmail.com>
 */

use Illuminate\Database\Eloquent\Model;
class Pago extends Model {

    protected $table = 'Pagos';

    public $codigopago;
    public $importe;
    public $fecha;

    public function getCodigopago()
    {
    	return $this->codigopago;
    }

    public function getImporte()
    {
    	return $this->importe;
    }

    public function getFecha()
    {
    	return $this->fecha;
    }

    public function setCodigopago($codigopago)
    {
    	return $this->codigopago = $codigopago;
    }

    public function setImporte($importe)
    {
    	return $this->importe = $importe;
    }

    public function setFecha($fecha)
    {
    	return $this->fecha = $fecha;
    }

}
