<?php namespace GeoPagos\Models\Entities;         

/**
 * @author Ricardo Bianchi <ericardo.bianchi@gmail.com>
 */

use Illuminate\Database\Eloquent\Model;
class Usuario extends Model {

    protected $table = 'Usuarios';

    public $codigousuario;
    public $usuario;
    public $clave;
    public $edad;

    public function getCodigousuario()
    {
    	return $this->codigousuario;
    }

    public function getUsuario()
    {
    	return $this->usuario;
    }

    public function getClave()
    {
    	return $this->clave;
    }

    public function getEdad()
    {
    	return $this->edad;
    }

    public function setCodigousuario($codigousuario)
    {
    	return $this->codigousuario = $codigousuario;
    }

    public function setUsuario($usuario)
    {
    	return $this->usuario = $usuario;
    }

    public function setClave($clave)
    {
    	return $this->clave = $clave;
    }

    public function setEdad($edad)
    {
    	return $this->edad = $edad;
    }

}
