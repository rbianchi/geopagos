<?php namespace GeoPagos\Models\Repositories;
/**
 * @author Ricardo Bianchi <ericardo.bianchi@gmail.com>
 */

use DB;
class DBConnection {

    private $dbConnection;
    public function __construct() {
        $this->dbConnection = '';

        if(DB::connection()->getDatabaseName())
		{
		   $this->dbConnection = DB::connection();
		}
    }


}
