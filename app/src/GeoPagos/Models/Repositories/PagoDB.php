<?php namespace GeoPagos\Models\Repositories;
/**
 * @author Ricardo Bianchi <ericardo.bianchi@gmail.com>
 */
use GeoPagos\Models\Repositories\DBConnection;
use GeoPagos\Models\Entities\Pago;
use GeoPagos\Models\Entities\UsuarioPagos;
use GeoPagos\Models\Exception\DBException;
use DB;

class PagoDB {

    public function __construct() {
    }

    public function listPagos() 
    {
        try {
            $results = Pago::all();
        } catch (\Illuminate\Database\QueryException $e) {
            throw new DBException("DB List Pagos exception");
        }
        return $results;
    }

    public function getPagoByCodigoPago($codigopago)
    {
        try {
            $result = Pago::where('codigopago', '=', $codigopago)->get();
        } catch (\Illuminate\Database\QueryException $e) {
            throw new DBException("DB getPagoByCodigoPago exception");
        }
        return $result;
    }

    public function add(Pago $pago, $codigousuario)
    {
        DB::beginTransaction(); 

        try {
            DB::insert('insert into Pagos (importe, fecha) values (?, ?)', 
                array(
                    $pago->getImporte(),
                    $pago->getFecha()
                )
            );

            $lastId = DB::select('select max(codigopago) as codigo from Pagos');
            DB::insert('insert into UsuariosPagos (codigopago, codigousuario) values (?, ?)', 
                array(
                    $lastId[0]->codigo,
                    $codigousuario
                )
            );
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            throw new DBException("DB Add Pago exception");
        }

        DB::commit();
    }

    public function remove($codigopago)
    {   
        DB::beginTransaction(); 

        $exist = $this->getPagoByCodigoPago($codigopago);
        if ( !isset($exist[0]) )
        {
            throw new DBException("DB remove Pago no existe");
        }

        try {
            DB::delete('delete from UsuariosPagos where codigopago = ?', 
                array(
                    $codigopago
                )
            );

            DB::delete('delete from Pagos where codigopago = ?', 
                array(
                    $codigopago
                )
            );
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            throw new DBException("DB Remove Pagos exception");
        }

        DB::commit();
    }

}
