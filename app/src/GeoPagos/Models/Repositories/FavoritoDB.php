<?php namespace GeoPagos\Models\Repositories;
/**
 * @author Ricardo Bianchi <ericardo.bianchi@gmail.com>
 */
use GeoPagos\Models\Repositories\DBConnection;
use GeoPagos\Models\Entities\Favorito;
use GeoPagos\Models\Exception\DBException;
use DB;

class FavoritoDB {

    public function __construct() {
    }

    public function listFavoritos() 
    {
        try {
            $results = Favorito::all();
        } catch (\Illuminate\Database\QueryException $e) {
            throw new DBException("DB List Favorito exception");
        }
        return $results;
    }

    public function getFavoritoByCodigoUsuario(Favorito $favorito)
    {
        try {
            $result = Favorito::where('codigousuario', '=', $favorito->getCodigousuario())
                ->where('codigousuariofavorito', '=', $favorito->getCodigousuariofavorito())
                ->get();
        } catch (\Illuminate\Database\QueryException $e) {
            throw new DBException("DB getFavoritoByCodigoUsuario exception");
        }
        return $result;
    }

    public function add(Favorito $favorito)
    {
        $exist = $this->getFavoritoByCodigoUsuario($favorito);
        if ( isset($exist[0]) )
        {
            throw new DBException("DB add Favorito ya existe");
        }

        try {
            DB::insert('insert into Favoritos (codigousuario, codigousuariofavorito) values (?, ?)', 
                array(
                    $favorito->getCodigousuario(),
                    $favorito->getCodigousuariofavorito()
                )
            );
        } catch (\Illuminate\Database\QueryException $e) {
            throw new DBException("DB Add Favorito exception");
        }
    }
 
    public function remove(Favorito $favorito)
    {   
        $exist = $this->getFavoritoByCodigoUsuario($favorito);
        if ( !isset($exist[0]) )
        {
            throw new DBException("DB remove Favorito no existe");
        }

        try {
            DB::delete('delete from Favoritos where codigousuario = ? and codigousuariofavorito = ?', 
                array(
                    $favorito->getCodigousuario(),
                    $favorito->getCodigousuariofavorito()
                )
            );
        } catch (\Illuminate\Database\QueryException $e) {
            throw new DBException("DB Remove Usuarios exception");
        }
    }

}
