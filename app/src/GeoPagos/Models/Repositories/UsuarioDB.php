<?php namespace GeoPagos\Models\Repositories;
/**
 * @author Ricardo Bianchi <ericardo.bianchi@gmail.com>
 */
use GeoPagos\Models\Repositories\DBConnection;
use GeoPagos\Models\Entities\Usuario;
use GeoPagos\Models\Exception\DBException;
use DB;

class UsuarioDB {

    public function __construct() {
    }

    public function listUsers() 
    {
        try {
            $results = Usuario::all();
        } catch (\Illuminate\Database\QueryException $e) {
            throw new DBException("DB List Usuarios exception");
        }
        return $results;
    }

    public function getUserByCodigoUsuario($codigousuario)
    {
        try {
            $result = Usuario::where('codigousuario', '=', $codigousuario)->get();
        } catch (\Illuminate\Database\QueryException $e) {
            throw new DBException("DB getUserByCodigoUsuario exception");
        }
        return $result;
    }

    public function getUserByUsuario($usuario)
    {
        try {
            $result = Usuario::where('usuario', '=', $usuario)->get();
        } catch (\Illuminate\Database\QueryException $e) {
            throw new DBException("DB getUserByUsuario exception");
        }
        return $result;
    }

    public function add(Usuario $usuario)
    {
        $exist = $this->getUserByUsuario($usuario->getUsuario());
        if ( isset($exist[0]) )
        {
            throw new DBException("DB El usuario ya existe");
        }

        try {
            DB::insert('insert into Usuarios (usuario, clave, edad) values (?, ?, ?)', 
                array(
                    $usuario->getUsuario(),
                    $usuario->getClave(),
                    $usuario->getEdad()
                )
            );
        } catch (\Illuminate\Database\QueryException $e) {
            throw new DBException("DB Add Usuarios exception");
        }
    }
 
    public function update(Usuario $usuario)
    {
        $exist = $this->getUserByCodigoUsuario($usuario->getCodigousuario());
        if ( !isset($exist[0]) )
        {
            throw new DBException("DB Update Usuario no existe");
        }

        try {
            DB::update('update Usuarios set clave = ?, edad = ? where codigousuario = ?', 
                array(
                    $usuario->getClave(),
                    $usuario->getEdad(),
                    $usuario->getCodigousuario()
                )
            );
        } catch (\Illuminate\Database\QueryException $e) {
            throw new DBException("DB Update Usuarios exception");
        }
    }

    public function remove($codigousuario)
    {
        $exist = $this->getUserByCodigoUsuario($usuario->getCodigousuario());
        if ( !isset($exist[0]) )
        {
            throw new DBException("DB Update Usuario no existe");
        }

        try {
            DB::delete('delete from Usuarios where codigousuario = ?', 
                array(
                    $codigousuario
                )
            );
        } catch (\Illuminate\Database\QueryException $e) {
            throw new DBException("DB Remove Usuarios exception");
        }
    }

}
