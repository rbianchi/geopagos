<?php namespace GeoPagos\Models\Services;
/**
 * @author Ricardo Bianchi <ericardo.bianchi@gmail.com>
 */
use GeoPagos\Models\Repositories\FavoritoDB;
use GeoPagos\Models\Entities\Favorito;
use GeoPagos\Models\Exception\ServiceException;
use GeoPagos\Models\Exception\DBException;
use GeoPagos\Models\Services\UsuarioService;
use Validator;

class FavoritoService {

    protected $favoritoDB = null; 
    protected $usuarioService = null;

    public function __construct() {
        $this->favoritoDB = new FavoritoDB();
        $this->usuarioService = new UsuarioService();
    }

    public function listFavoritos()
    {
        try {
            return $this->favoritoDB->listFavoritos();
        } catch ( DBException $de ) {
            throw new ServiceException($de->getMessage());
        } 
    }

    public function add(Favorito $favorito) 
    {
        $this->validarFavorito($favorito);
        
        $exist = $this->usuarioService->getUserByCodigoUsuario($favorito->getCodigousuario());
        if ( isset($exist[0]) ) {
            $exist = $this->usuarioService->getUserByCodigoUsuario($favorito->getCodigousuariofavorito());
            if ( !isset($exist[0]) ) {
                throw new ServiceException("El usuario con codigousuariofavorito no existe");
            }
        } else {
            throw new ServiceException("El usuario con codigousuario no existe");
        }

        try {
            $this->favoritoDB->add($favorito);
        } catch ( DBException $de ) {
            throw new ServiceException($de->getMessage());
        } 
    }
 
    public function remove(Favorito $favorito)
    {
        $this->validarFavorito($favorito);

        try {
            $this->favoritoDB->remove($favorito);
        } catch ( DBException $de ) {
            throw new ServiceException($de->getMessage());
        } 
    }

    private function validarFavorito(Favorito $favorito) 
    {

        $input = array( 
            'codigousuario' => $favorito->getCodigousuario(),
            'codigousuariofavorito' => $favorito->getCodigousuariofavorito()
        );

        $rules = array(
            'codigousuario' => 'Required|integer',
            'codigousuariofavorito' => 'Required|integer'
        );

        $v = Validator::make($input, $rules);
        if( $v->passes() ) {
            return;
        } else {
            throw new ServiceException($v->messages());
        }

    }

}
