<?php namespace GeoPagos\Models\Services;
/**
 * @author Ricardo Bianchi <ericardo.bianchi@gmail.com>
 */
use GeoPagos\Models\Repositories\UsuarioDB;
use GeoPagos\Models\Entities\Usuario;
use GeoPagos\Models\Exception\ServiceException;
use GeoPagos\Models\Exception\DBException;
use Validator;

class UsuarioService {

    protected $usuarioDB = null; 
    
    public function __construct() {
        $this->usuarioDB = new UsuarioDB();
    }

    public function listUsers()
    {
        try {
            return $this->usuarioDB->listUsers();
        } catch ( DBException $de ) {
            throw new ServiceException($de->getMessage());
        } 
    }

    public function getUserByCodigoUsuario($codigousuario) {
        return $this->usuarioDB->getUserByCodigoUsuario($codigousuario);
    }

    public function add(Usuario $usuario) 
    {
        $this->validarAltaUsuario($usuario);
            
        try {
            $this->usuarioDB->add($usuario);
        } catch ( DBException $de ) {
            throw new ServiceException($de->getMessage());
        } 
    }
 
    public function update(Usuario $usuario)
    {
        $this->validarUpdateUsuario($usuario);

        try {
            $this->usuarioDB->update($usuario);
        } catch ( DBException $de ) {
            throw new ServiceException($de->getMessage());
        } 
    }

    public function remove($codigousuario)
    {
        $this->validarRemoveUsuario($codigousuario);

        try {
            $this->usuarioDB->remove($codigousuario);
        } catch ( DBException $de ) {
            throw new ServiceException($de->getMessage());
        } 
    }

    private function validarAltaUsuario(Usuario $usuario) 
    {

        $input = array( 
            'usuario' => $usuario->getUsuario(),
            'edad' => $usuario->getEdad()
        );

        $rules = array(
            'usuario' => 'Required|Min:1|Max:80|Alpha',
            'edad' => 'Required|integer|between:19,150'
        );

        $v = Validator::make($input, $rules);
        if( $v->passes() ) {
            return;
        } else {
            throw new ServiceException($v->messages());
        }

    }

    private function validarUpdateUsuario(Usuario $usuario) 
    {

        $input = array( 
            'codigousuario' => $usuario->getCodigousuario(),
            'edad' => $usuario->getEdad()
        );

        $rules = array(
            'codigousuario' => 'Required|integer',
            'edad' => 'integer|between:19,150'
        );

        $v = Validator::make($input, $rules);
        if( $v->passes() ) {
            return;
        } else {
            throw new ServiceException($v->messages());
        }

    }

    private function validarRemoveUsuario($codigousuario) 
    {
        $input = array( 
            'codigousuario' => $codigousuario
        );

        $rules = array(
            'codigousuario' => 'Required|integer'
        );

        $v = Validator::make($input, $rules);
        if( $v->passes() ) {
            return;
        } else {
            throw new ServiceException($v->messages());
        }
    }

}
