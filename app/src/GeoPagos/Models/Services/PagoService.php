<?php namespace GeoPagos\Models\Services;
/**
 * @author Ricardo Bianchi <ericardo.bianchi@gmail.com>
 */
use GeoPagos\Models\Repositories\PagoDB;
use GeoPagos\Models\Entities\Pago;
use GeoPagos\Models\Exception\ServiceException;
use GeoPagos\Models\Exception\DBException;
use GeoPagos\Models\Services\UsuarioService;
use Validator;

class PagoService {

    protected $pagoDB = null; 
    protected $usuarioService = null;

    public function __construct() {
        $this->pagoDB = new PagoDB();
        $this->usuarioService = new UsuarioService();
    }

    public function listPagos()
    {
        try {
            return $this->pagoDB->listPagos();
        } catch ( DBException $de ) {
            throw new ServiceException($de->getMessage());
        } 
    }

    public function add(Pago $pago, $codigousuario) 
    {
        $this->validarAltaPago($pago, $codigousuario);
        if ( $pago->getImporte() <= 0 )
        {
            throw new ServiceException("El importe debe ser mayor a cero");
        }

        $exist = $this->usuarioService->getUserByCodigoUsuario($codigousuario);
        if ( !isset($exist[0]) ) {
            throw new ServiceException("El usuario con codigousuario no existe");
        }

        $now = new \DateTime();
        $nowTimestamp = $now->getTimestamp();

        $date = date_create_from_format('d/m/Y', $pago->getFecha());
        $pagoTimestamp = $date->getTimestamp();

        if ( $pagoTimestamp < $nowTimestamp )
        {
            throw new ServiceException("La fecha del pago debe ser posterior a la fecha actual");
        }

        $pago->setFecha(date('Y-m-d', $pagoTimestamp));

        try {
            $this->pagoDB->add($pago, $codigousuario);
        } catch ( DBException $de ) {
            throw new ServiceException($de->getMessage());
        } 
    }
 
    public function remove($codigopago)
    {
        $this->validarRemovePago($codigopago);

        try {
            $this->pagoDB->remove($codigopago);
        } catch ( DBException $de ) {
            throw new ServiceException($de->getMessage());
        } 
    }

    private function validarAltaPago(Pago $pago, $codigousuario) 
    {
        $input = array( 
            'codigousuario' => $codigousuario,
            'fecha' => $pago->getFecha()
        );

        $rules = array(
            'codigousuario' => 'Required|integer',
            'fecha' => 'Required|date_format:d/m/Y'
        );

        $v = Validator::make($input, $rules);
        if( $v->passes() ) {
            return;
        } else {
            throw new ServiceException($v->messages());
        }

    }

    private function validarRemovePago($codigopago) 
    {
        $input = array( 
            'codigopago' => $codigopago
        );

        $rules = array(
            'codigopago' => 'Required|integer'
        );

        $v = Validator::make($input, $rules);
        if( $v->passes() ) {
            return;
        } else {
            throw new ServiceException($v->messages());
        }

    }
}
