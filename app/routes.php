<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


Route::get('user-list', array('as' => 'user-list', 'uses' => 'GeoPagos\Controllers\UsuariosController@listUsers'));
Route::post('user-add', array('as' => 'user-add', 'uses' => 'GeoPagos\Controllers\UsuariosController@add'));
Route::post('user-remove', array('as' => 'user-remove', 'uses' => 'GeoPagos\Controllers\UsuariosController@remove'));
Route::post('user-update', array('as' => 'user-update', 'uses' => 'GeoPagos\Controllers\UsuariosController@update'));

Route::get('favorito-list', array('as' => 'favorito-list', 'uses' => 'GeoPagos\Controllers\FavoritosController@listFavoritos'));
Route::post('favorito-add', array('as' => 'favorito-add', 'uses' => 'GeoPagos\Controllers\FavoritosController@add'));
Route::post('favorito-remove', array('as' => 'favorito-remove', 'uses' => 'GeoPagos\Controllers\FavoritosController@remove'));

Route::get('pago-list', array('as' => 'pago-list', 'uses' => 'GeoPagos\Controllers\PagosController@listPagos'));
Route::post('pago-add', array('as' => 'pago-add', 'uses' => 'GeoPagos\Controllers\PagosController@add'));
Route::post('pago-remove', array('as' => 'pago-remove', 'uses' => 'GeoPagos\Controllers\PagosController@remove'));

