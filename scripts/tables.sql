create table Usuarios (  codigousuario INT NOT NULL AUTO_INCREMENT, usuario VARCHAR(200) NOT NULL, clave VARCHAR(200), edad INT, PRIMARY KEY (codigousuario));

create table Favoritos (  codigousuario INT NOT NULL, codigousuariofavorito INT NOT NULL, PRIMARY KEY (codigousuario, codigousuariofavorito), FOREIGN KEY (codigousuario) REFERENCES Usuarios(codigousuario), FOREIGN KEY (codigousuariofavorito) REFERENCES Usuarios(codigousuario));

create table Pagos ( codigopago INT NOT NULL AUTO_INCREMENT, importe DECIMAL(12,2) NOT NULL, fecha TIMESTAMP, PRIMARY KEY (codigopago));

create table UsuariosPagos ( codigopago INT NOT NULL , codigousuario INT NOT NULL, PRIMARY KEY (codigopago, codigousuario), FOREIGN KEY (codigopago) REFERENCES Pagos(codigopago), FOREIGN KEY (codigousuario) REFERENCES Usuarios(codigousuario));
